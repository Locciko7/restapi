//
//  Models.swift
//  DetailsListAboutFootball
//
//  Created by Loci Olah on 29.10.2021.
//

import Foundation
import UIKit

struct TeamRespModel: Codable {
    let response: [TeamModel]
}

struct TeamModel: Codable {
    let team: Info
    let venue: Venue
}

struct Info: Codable {
    let id: Int
    let name: String
    let country: String
    let founded: Int
    let national: Bool
    let logo: String
}

struct Venue: Codable {
    let id: Int
    let name: String
    let address: String
    let city: String
    let capacity: Int
    let surface: String
    let image: String
}

struct PlayerRespModel: Codable {
    let response: [PlayerGlobal]
}

struct PlayerGlobal: Codable {
    let player: Player
    let statistics: [Statistics]
}

struct Player: Codable {
    let age: Int
    let birth: Birthday
    let firstname:String
    let height: String
    let id: Int
    let injured: Bool
    let lastname: String
    let name: String
    let nationality: String
    let photo: String
    let weight: String
}

struct Statistics: Codable {
    let team: Team
}

struct Team: Codable {
    let id: Int
    let logo: String
    let name: String
}

struct Birthday: Codable {
    let country: String
    let date: String
    let place: String
}
