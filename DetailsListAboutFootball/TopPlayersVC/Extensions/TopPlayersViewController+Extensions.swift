//
//  TopPlayersViewController+Extensions.swift
//  DetailsListAboutFootball
//
//  Created by Loci Olah on 29.10.2021.
//

import Foundation
import UIKit

extension TopPlayersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: topPlayersListCellID, for: indexPath) as! TopListCell
        cell.update(playerGlobal: playersList[indexPath.row])
        cell.tag = indexPath.row
        cell.delegate = self
        return cell
    }
}

extension TopPlayersViewController: DetailsCellDelegate {
    
    func nameSelected(index: Int) {
        
        let playerTeamInfoVC = PlayerDetailsViewController(nibName: "PlayerDetailsViewController", bundle: nil)
        playerTeamInfoVC.player = playersList[index]
        navigationController?.pushViewController(playerTeamInfoVC, animated: true)
    }
    
    func teamSelected(index: Int) {
        
        let playerTeamInfoVC = TeamDetailsViewController(nibName: "TeamDetailsViewController", bundle: nil)
        playerTeamInfoVC.team_id = playersList[index].statistics[0].team.id
        navigationController?.pushViewController(playerTeamInfoVC, animated: true)
    }
}
