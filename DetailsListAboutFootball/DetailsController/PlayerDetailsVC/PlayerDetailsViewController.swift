//
//  PlayerDetailsViewController.swift
//  DetailsListAboutFootball
//
//  Created by Loci Olah on 29.10.2021.
//

import UIKit

class PlayerDetailsViewController: UIViewController {
    
    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var playerAgeLabel: UILabel!
    @IBOutlet weak var playerHeightLabel: UILabel!
    @IBOutlet weak var playerNationelityLabel: UILabel!

        var player: PlayerGlobal?
        var team_id: Int?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            guard player != nil else {
                return
            }
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
            guard team_id != nil else {
                return
            }
            
            NetworkingManager().getPlayer(league: "39", season: "2019") { players, error in
                
                guard error == nil else {
                    self.present(Alert().showAlert(title: "Error", message: error, buttonTitle: "cancel"), animated: true, completion: nil)
                    return
                }

                guard players != nil else {
                    self.present(Alert().showAlert(title: "Error", message: error, buttonTitle: "cancel"), animated: true, completion: nil)
                    return
                }
                
                DispatchQueue.main.async {
                    
                    guard let player = self.player else {
                        return
                    }
                    self.playerAgeLabel.text = String(player.player.age)
                    self.playerHeightLabel.text = self.player?.player.height
                    self.playerNationelityLabel.text = player.player.nationality
                    if let imagePlayerUrl = URL(string: player.player.photo) {
                        self.loadImage(url: imagePlayerUrl)
                    }
                }
            }
        }
        
    @IBAction func back(_ sender: Any) {
            navigationController?.popViewController(animated: true)
        }
    }

extension PlayerDetailsViewController {
    
    private func loadImage(url: URL) {
        
        DispatchQueue.global().async {
            
            if let data = try? Data(contentsOf: url) {
                
                if let image = UIImage(data: data) {
                    
                    DispatchQueue.main.async { [self] in
                        
                        playerImage.image = image
                    }
                }
            }
        }
    }
}
