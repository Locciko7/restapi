//
//  TeamDetailsViewController.swift
//  DetailsListAboutFootball
//
//  Created by Loci Olah on 29.10.2021.
//

import UIKit

class TeamDetailsViewController: UIViewController {
    
    @IBOutlet weak var teamLogoImageView: UIImageView!
    @IBOutlet weak var countryTextLabel: UILabel!
    @IBOutlet weak var stadiumTextLabel: UILabel!
    @IBOutlet weak var adressTextLabel: UILabel!
    
    var team: TeamModel?
    var team_id: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        guard let team_id = team_id else {
            return
        }
        
        NetworkingManager().getTeam(league: "39", season: "2019", team_id: team_id) { team, error in
            
            guard error == nil else {
                self.present(Alert().showAlert(title: "Error", message: error, buttonTitle: "cancel"), animated: true, completion: nil)
                return
            }
            
            guard let team = team else {
                return
            }
            
            self.team = team
            
            DispatchQueue.main.async {
                
                self.countryTextLabel.text = team.team.country
                self.stadiumTextLabel.text = team.venue.name
                self.stadiumTextLabel.text = team.venue.address
                if let imageURL = URL(string: team.team.logo) {
                    self.loadImage(url: imageURL)
                }
            }
        }
    }

    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension TeamDetailsViewController {
    
    private func loadImage(url: URL) {
        
        DispatchQueue.global().async {
            
            if let data = try? Data(contentsOf: url) {
                
                if let image = UIImage(data: data) {
                    
                    DispatchQueue.main.async { [self] in
                        
                        teamLogoImageView.image = image
                    }
                }
            }
        }
    }
}
