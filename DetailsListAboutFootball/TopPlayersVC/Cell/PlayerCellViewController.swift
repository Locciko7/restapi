//
//  PlayerCellViewController.swift
//  DetailsListAboutFootball
//
//  Created by Loci Olah on 29.10.2021.
//

import UIKit

protocol DetailsCellDelegate {
    func nameSelected(index: Int)
    func teamSelected(index: Int)
}

class TopListCell: UITableViewCell {
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var teamLabel: UILabel!
    
    var delegate: DetailsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func update(playerGlobal: PlayerGlobal){
        nameLabel.text = playerGlobal.player.name
        teamLabel.text = playerGlobal.statistics[0].team.name
    }
    
    @IBAction func nameSelected(_ sender: Any) {
        delegate?.nameSelected(index: self.tag)
    }
    
    @IBAction func teamSelected(_ sender: Any) {
        delegate?.teamSelected(index: self.tag)
        
        }
    }
