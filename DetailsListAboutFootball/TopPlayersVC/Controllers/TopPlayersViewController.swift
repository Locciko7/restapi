//
//  TopPlayersViewController.swift
//  DetailsListAboutFootball
//
//  Created by Loci Olah on 29.10.2021.
//

import UIKit

class TopPlayersViewController: UIViewController {
    
    let topPlayersListCellID = String(describing: TopPlayersViewController.self)
    var playersList = [PlayerGlobal]()
    var season: String = ""
    var league: String = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: topPlayersListCellID, bundle: nil), forCellReuseIdentifier: topPlayersListCellID)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NetworkingManager().getPlayer(league: league, season: season) { players, error in
            
            guard error == nil else {
                self.present(Alert().showAlert(title: "Error", message: error, buttonTitle: "cancel"), animated: true, completion: nil)
                return
            }

            guard players != nil else {
                self.present(Alert().showAlert(title: "Error", message: error, buttonTitle: "cancel"), animated: true, completion: nil)
                return
            }
        }
    }
}
