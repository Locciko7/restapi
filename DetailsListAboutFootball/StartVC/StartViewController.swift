//
//  StartViewController.swift
//  DetailsListAboutFootball
//
//  Created by Loci Olah on 29.10.2021.
//

import UIKit

class StartViewController: UIViewController {

    @IBOutlet weak var leagueTextField: UITextField!
    
    @IBOutlet weak var seasonTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func startButton(_ sender: Any) {
        
        guard
            let league = leagueTextField.text, let season = seasonTextField.text, !league.isEmpty, !season.isEmpty, (Int(league) != nil), (Int(season) != nil) else {
            self.present(Alert().showAlert(title: "Error", message: "All fields have to be entered\nCheck entered text", buttonTitle: "cancel"), animated: true, completion: nil)
            return
        }
        
        let playersListVC = TopPlayersViewController(nibName: "TopPlayersViewController", bundle: nil)
        playersListVC.league = league
        playersListVC.season = season
        navigationController?.pushViewController(playersListVC, animated: true)
    }
}
