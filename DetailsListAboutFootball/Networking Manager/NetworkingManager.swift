//
//  NetworkingManager.swift
//  DetailsListAboutFootball
//
//  Created by Loci Olah on 29.10.2021.
//

import Foundation

class NetworkingManager {
    
    static let shared = NetworkingManager()
    let playersUrl = "https://api-football-beta.p.rapidapi.com/players/topscorers"
    let teamsUrl = "https://api-football-beta.p.rapidapi.com/teams"
    let apiHost = "api-football-beta.p.rapidapi.com"
    let apiKey = "ca6d1089c4msh40fccbaae486721p1189eejsn6b52ab97c284"
    
    func getPlayer(league: String, season: String, completionHandler: @escaping ([PlayerGlobal]?, String?) -> Void) {
        
        let urlComponents = URLComponents(string: playersUrl)
        
        guard var urlComponents = urlComponents else { return }
        
        let queryItemLeague = URLQueryItem(name: "league", value: league)
        let queryItemSeason = URLQueryItem(name: "season", value: season)
        
        urlComponents.queryItems = [queryItemLeague, queryItemSeason]
        
        guard let url = urlComponents.url else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["X-RapidAPI-Host" : apiHost,
                                       "X-RapidAPI-Key" : apiKey]
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                completionHandler(nil, error?.localizedDescription)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(PlayerRespModel.self, from: data)
                completionHandler(response.response, nil)
            
            } catch let errorDecode as NSError {
                completionHandler(nil, errorDecode.localizedDescription)
            }
        }.resume()
    }
    
    func getTeam(league: String, season: String, team_id: Int, completionHandler: @escaping (TeamModel?, String?) -> Void) {
        
        let urlComponents = URLComponents(string: teamsUrl)
        
        guard var urlComponents = urlComponents else { return }
        
        let queryItemLeague = URLQueryItem(name: "league", value: league)
        let queryItemSeason = URLQueryItem(name: "season", value: season)
        
        urlComponents.queryItems = [queryItemLeague, queryItemSeason]
        
        guard let url = urlComponents.url else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["X-RapidAPI-Host" : apiHost,
                                       "X-RapidAPI-Key" : apiKey]
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                completionHandler(nil, error?.localizedDescription)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(TeamRespModel.self, from: data)
                
                for team in response.response {
                    if team.team.id == team_id {
                        completionHandler(team, nil)
                    }
                }
            
            } catch let errorDecode as NSError {
                completionHandler(nil, errorDecode.localizedDescription)
            }
        }.resume()
    }
}
